# POC GITLAB CI

## gitlab init project
```
git init --initial-branch=main
git remote add origin git@gitlab.com:your_gitlab_user_or_groupname_here/gitlabci_02.git
git add .
git commit -m "Initial commit"
git push -u origin main
```

## enable CI/CD for gitlab project
see https://docs.gitlab.com/ee/ci/enable_or_disable_ci.html#enable-cicd-in-a-project  

## force success on pipeline for merge request
in your project go to settings/merge_requests, fin Merge checks, check button 'Pipelines must succeed'  
then click button "save changes" at the bottom of the page  
see https://gitlab.com/skeres/gitlabci_02/-/settings/merge_requests  

## manual stage
les "stages" manuels bloquent les merges request. Il faut les run manuellement pour faire  
aboutir la merge request  

## comportement sur l'obligation de succès de la piepeline pour une merge-request  
La pipeline est déclenchée une première fois pour autoriser le merge de develop sur main,  
puis une seconde fois sur la branche main après le merge.  ? pourquoi

## ATTENTION : option à setter obligatoirement sur le projet
aller sur https://gitlab.com/skeres/cicd02/-/blob/main/jobs/java/compile_artefacts.yml  
Décocher : Enable "Delete source branch" option by default  

## note sur la protection des branches : merge, push, delete ...   
voir https://docs.gitlab.com/ee/user/project/protected_branches.html#for-a-project  

## comportement sur branche feature à reporter ici 
Sur un push d'une branche feature : la pipeline ne devrait pas se déclencher avec  
un push simple de même nom de branche  