package com.example.demo.utils;

import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@Component
public class Utilitaires {

    /**
     * return current datetime string formated
     * @return
     */
    public String getCurrentDateTime() {
        Date d  = Calendar.getInstance().getTime();
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(d);
    }
}
