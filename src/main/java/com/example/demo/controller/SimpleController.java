package com.example.demo.controller;

import org.slf4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.slf4j.LoggerFactory.getLogger;

@RestController
@RequestMapping("/")
public class SimpleController {

    private static final Logger logger = getLogger("com.log.controller");

    private static final String ROOT_LOG_MESSAGE="project gitlabci|controller";
    private static final String SEPARATOR="|";

    @GetMapping("/")
    public String getRoot() {
        logger.debug(ROOT_LOG_MESSAGE+SEPARATOR+"hello"+SEPARATOR+getCurrentDateTime());
        return "hello from Controller at root path !";
    }

    @GetMapping("/hello")
    public String getHome() {
        logger.debug(ROOT_LOG_MESSAGE+SEPARATOR+"hello"+SEPARATOR+getCurrentDateTime());
        return "hello from Controller !";
    }

    @GetMapping("/date")
    public String getDate() {
        logger.debug(ROOT_LOG_MESSAGE+SEPARATOR+"date"+SEPARATOR+getCurrentDateTime());
        return "Current datetime is " + getCurrentDateTime();
        }

    /**
     * return current datetime string formated
     * @return
     */
    public String getCurrentDateTime() {
        Date d  = Calendar.getInstance().getTime();
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(d);
        }

}
