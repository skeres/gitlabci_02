package com.example.demo.utils;

import com.example.demo.controller.SimpleController;
import org.junit.jupiter.api.Test;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class UtilitairesTest {

        @Test
        void dateDuJourEqualToDay() {
                Utilitaires utilitaires = new Utilitaires();
                Date d  = Calendar.getInstance().getTime();
                String dateSdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(d);
                String expectedResultForExtractDay=dateSdf.split(" ")[0];
                assertEquals(expectedResultForExtractDay, utilitaires.getCurrentDateTime().split(" ")[0]);
        }

}
