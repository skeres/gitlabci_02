package com.example.demo.controller;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;


public class SimpleControllerTest {

        @Test
        void getHelloShouldReturnDefaultMessage() {
                SimpleController simpleController = new SimpleController();
                assertEquals("hello from Controller !", simpleController.getHome());
        }

}
